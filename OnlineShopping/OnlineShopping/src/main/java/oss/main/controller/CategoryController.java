package oss.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import oss.main.model.Category;
import oss.main.service.CategoryService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CategoryController {

	@Autowired
	CategoryService categoryService;
	
	@PostMapping("/category")
	public void addCategory(@RequestBody Category category) {
		categoryService.addCatogory(category);
	}
	
	@GetMapping("/getcategory")
	public List<Category> getCategory() {
		return categoryService.getCategory();
		
	}
}
