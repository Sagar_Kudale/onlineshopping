package oss.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import oss.main.model.Login;
import oss.main.model.Product;
import oss.main.repository.LoginRepository;
import oss.main.service.LoginService;
import oss.main.service.ProductService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LoginController {
	@Autowired
	LoginService loginService;
	
	//@Autowired
	//LoginRepository loginreposi;
	
	@PostMapping("/login")
	public void addlogin(@RequestBody Login login) {
		loginService.saveLogin(login);
		//loginreposi.save(login);
	}
}
