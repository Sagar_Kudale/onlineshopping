package oss.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import oss.main.model.Product;
import oss.main.service.ProductService;

@RestController
public class ProductController {
	@Autowired
	ProductService productService;
	
	@PostMapping("/saveproducts")
	public void addProduct(@RequestBody Product product) {
		productService.save(product);
	}
	
	@PostMapping("/getproductbycategory")
	public List<Product> getProductByCategory(@RequestBody Long id) {
		return productService.getProduct(id);
	}
	
}
 