package oss.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import oss.main.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

	
}
