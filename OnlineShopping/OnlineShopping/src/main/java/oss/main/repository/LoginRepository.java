package oss.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import oss.main.model.Login;


@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

}
