package oss.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oss.main.model.Category;
import oss.main.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	public void addCatogory(Category category) {
		categoryRepository.save(category);
		
	}

	public List<Category> getCategory() {
		return categoryRepository.findAll();
		
		
	}

}
