package oss.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oss.main.model.Login;
import oss.main.repository.LoginRepository;
import oss.main.repository.ProductRepository;

@Service
public class LoginService {
	@Autowired
	LoginRepository loginRepository;
	
	public void saveLogin(Login login) {
		System.out.println(login);
		loginRepository.save(login);
		
	}

}
