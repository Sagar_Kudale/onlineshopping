package oss.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oss.main.model.Product;
import oss.main.repository.ProductRepository;


@Service
public class ProductService {
	@Autowired
	ProductRepository productRepository;
	
	public void save(Product product) {
		 productRepository.save(product);
	}

	public List<Product> getProduct(Long id) {
		
		return productRepository.findByCategory(id);
	}

	
}
