import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component';
import { RouterModule } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { ProductService } from './services/ProductService';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { ShippingComponent } from './components/shipping/shipping.component';
@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CartComponent,
    NavbarComponent,
    LoginComponent,
    ShippingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'products', component: ProductComponent },
      { path: 'cart', component: CartComponent },
      { path: '', component: LoginComponent },
      { path: 'shipping', component: ShippingComponent },
      { path: '**', redirectTo: '' }
    ])
  ],
  providers: [ProductService,HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
