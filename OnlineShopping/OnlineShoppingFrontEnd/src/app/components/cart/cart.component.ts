import { Component, OnInit } from '@angular/core';
import { Item } from '../../entities/item';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/ProductService';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  

	constructor(
    private cartService: CartService
  ) { }
  
  items;

	ngOnInit() {
    this.items = this.cartService.getItems();
  }
}
