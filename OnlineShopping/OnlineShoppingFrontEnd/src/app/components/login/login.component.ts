import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/entities/login';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login : Login;
  constructor(private route: ActivatedRoute, 
    private router: Router, 
      private loginService: LoginService) {
        this.login = new Login();
      //  this.login.username = 'sagar';
       }

    onSubmit() {
        console.log("usrname is"+this.login.username)
        console.log("password is"+this.login.password)
        this.loginService.saveLogin(this.login).subscribe(result => this.gotoUserList());
      }
     
      gotoUserList() {
        this.router.navigate(['/products']);
      }
  ngOnInit() {
  }

}
