import { Component, OnInit } from '@angular/core';
import { Product } from '../../entities/Product';
import { ProductService } from '../../services/ProductService';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '../../services/cart.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  private products: Product[];

	constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private cartService: CartService
  ) { }
  
 

	ngOnInit() {
    this.products = this.productService.findAll();
	}

     
  addToCart(product) {
    this.cartService.addToCart(product);
    window.alert('Your product has been added to the cart!');
  }
}

