import { Product } from '../entities/Product';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//import { Observable } from 'rxjs/Observable';
@Injectable()
export class ProductService{

    private productsUrl : string;
    private products : Product[];

    constructor(/*rivate http: HttpClient*/){
        this.productsUrl = 'http://localhost:8080/saveproducts';
        this.products
         = [
            { id: 'p01', name: 'BMW', price: 100, pictureUrl: 'C:/Users/sagar/Desktop/messi5.jpg' },
            { id: 'p02', name: 'AUDI', price: 200, pictureUrl: 'thumb2.gif' },
            { id: 'p03', name: 'MERCEDES', price: 300, pictureUrl: 'thumb3.gif' }
        ];
    }

    findAll(): Product[]{
        return this.products;
    }

/*findAll(): Observable<Product[]>{
        return this.http.get<Product[]>(this.productsUrl);
    }*/

    find(id:string) : Product{
        return this.products[this.getSelectedIndex[id]];
    }

    getSelectedIndex(id:string){
        for(var i = 0 ; i < this.products.length ; i++){
            if(this.products[i].id == id){
                return i;
            }
        }
        return -1;
    }
}