import { Injectable } from '@angular/core';
import { Category } from '../entities/category';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private categoryUrl : string;
  constructor(private http: HttpClient)
   {
    this.categoryUrl = 'http://localhost:8080/getcategory';
    }

  public findAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoryUrl);
  }

}
