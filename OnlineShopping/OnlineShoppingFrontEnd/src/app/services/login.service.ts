import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../entities/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginUrl : string;
  constructor(private http : HttpClient) {
    this.loginUrl = 'http://localhost:8080/login'; 
     }

     public saveLogin(login : Login){
       console.log(login.username);
      return this.http.post<Login>(this.loginUrl, login);
     }
}
