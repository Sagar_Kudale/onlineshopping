package oss.main.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import oss.main.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
	List<Customer> findByFirstName(String firstName);	
	Optional<Customer> findById(Long id);
}
